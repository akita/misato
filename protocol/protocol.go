// Package protocol defines the protocol used by the server, submitter, and
// runner.
package protocol

import "time"

type TaskStatus int

const (
	TaskStatusInvaid TaskStatus = iota
	TaskStatusWaitingForUploadingArtifacts
	TaskStatusReady
	TaskStatusRunning
	TaskStatusCompleted
)

func (s TaskStatus) String() string {
	switch s {
	case TaskStatusInvaid:
		return "invalid"
	case TaskStatusWaitingForUploadingArtifacts:
		return "waiting_for_uploading_artifacts"
	case TaskStatusReady:
		return "ready"
	case TaskStatusRunning:
		return "running"
	case TaskStatusCompleted:
		return "completed"
	default:
		return "unknown"
	}
}

// A task is a program to run on the runner.
type Task struct {
	ID        int64      `json:"id,omitempty"`
	CreatedAt time.Time  `json:"created_at,omitempty"`
	UpdatedAt time.Time  `json:"updated_at,omitempty"`
	Command   string     `json:"command,omitempty"`
	Status    TaskStatus `json:"status,omitempty"`
	Output    string     `json:"output,omitempty"`
}
