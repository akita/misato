# Misato 

Misato is a tool that can execute Akita simulations remotely. Misato involves at least 3 machines, the submitter, the server, and the runners. The submitter submits the task to the server. The runners keep pulling tasks from the server and execute them. 

## How to use

### Server

Install Misato server:

```bash
go install gitlab.com/akita/misato/misato_server@latest
```

Run the server: 
```bash
misato_server
```

By default, the server runs on port 7532. If another ports is needed, use the `-p` option:

```bash
misato_server -p 7532
```

### Runner

Install Misato runner:

```bash
go install gitlab.com/akita/misato/misato_runner@latest
```

Start the runner: 
```bash
misato_runner 
```

The server URL needs to be specified using environment variable `MISATO_SERVER_URL`. Setting the environment is required. 

It is recommended to run the runner as a background process. One way is to use tools such as Tmux. Or you can use `nohup` to run the runner in the background.

### Submitter

Install the Misato submitter:

```bash
go install gitlab.com/akita/misato/misato_submitter@latest
```

Here is how to submit a task. Suppose we need to run the fir program with parameter `-timing`, you can submit the task with 

```bash
misato_submitter -- fir -timing
```

Similar to the runner, the server URL needs to be specified using environment variable `MISATO_SERVER_URL`. The environment is also required. 

The submitter works by zipping the current directory and sending it to the server. The runner will download the zip file and unzip it. Therefore, the current directory must be self-contained. Also, large files should be avoided in the directory. 



