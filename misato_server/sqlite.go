package main

import (
	"database/sql"
	"time"

	_ "github.com/mattn/go-sqlite3" // Import go-sqlite3 library
	"gitlab.com/akita/misato/protocol"
)

// SQLite provides a Database implementation based on SQLite.
type SQLite struct {
	db *sql.DB
}

// Init establishes a connection to the database.
func (s *SQLite) Init() error {
	db, err := sql.Open("sqlite3", "./akita.db")
	if err != nil {
		return err
	}

	s.db = db

	s.createTables()

	return nil
}

func (s *SQLite) createTables() {
	sql := `
		CREATE TABLE IF NOT EXISTS task (
			id 				INTEGER 	PRIMARY KEY AUTOINCREMENT,
			command 		TEXT 		NOT NULL,
			created_at 		TIMESTAMP 	NOT NULL,
			updated_at 		TIMESTAMP 	,
			status 			INTEGER 		,
			output 			TEXT 		
		);
	`

	_, err := s.db.Exec(sql)
	if err != nil {
		panic(err)
	}
}

// AddTask adds a task to the database.
func (s *SQLite) AddTask(t protocol.Task) (int64, error) {
	sql := `
		INSERT INTO task (
			command,
			created_at,
			updated_at,
			status,
			output
		) VALUES (
			?,
			?,
			?,
			?,
			?
		);
	`

	res, err := s.db.Exec(sql,
		t.Command, t.CreatedAt, t.UpdatedAt, t.Status, t.Output)
	if err != nil {
		return -1, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return -1, err
	}

	return id, nil
}

// UpdateTask updates the task in the database.
func (s *SQLite) UpdateTask(t protocol.Task) error {
	sql := `
		UPDATE task SET
			updated_at = ?,
			status = ?,
			output = ?
		WHERE id = ?;
	`

	t.UpdatedAt = time.Now()

	_, err := s.db.Exec(sql,
		t.UpdatedAt, t.Status, t.Output, t.ID)
	if err != nil {
		return err
	}

	return nil
}

// GetTask returns the task with the given ID.
func (s *SQLite) GetTask(id int64) (protocol.Task, error) {
	var t protocol.Task

	sql := `
		SELECT
			id,
			command,
			created_at,
			updated_at,
			status,
			output
		FROM task
		WHERE id = ?;
	`

	err := s.db.QueryRow(sql, id).Scan(
		&t.ID, &t.Command, &t.CreatedAt, &t.UpdatedAt, &t.Status, &t.Output)
	if err != nil {
		return t, err
	}

	return t, nil
}

// GetNextTask returns the task that is ready and is created the earliest.
func (s *SQLite) GetNextTask() (protocol.Task, bool, error) {
	sql := `
		SELECT * FROM task
		WHERE status = ?
		ORDER BY created_at ASC
		LIMIT 1;
	`

	rows, err := s.db.Query(sql, protocol.TaskStatusReady)
	if err != nil {
		return protocol.Task{}, false, err
	}

	defer rows.Close()

	var task protocol.Task

	count := 0
	for rows.Next() {
		err = rows.Scan(
			&task.ID,
			&task.Command,
			&task.CreatedAt,
			&task.UpdatedAt,
			&task.Status,
			&task.Output)
		if err != nil {
			return protocol.Task{}, true, err
		}

		count++
	}

	if count == 0 {
		return protocol.Task{}, false, nil
	}

	return task, true, nil
}
