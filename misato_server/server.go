package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/akita/misato/protocol"
)

var portFlag = flag.String("port", "7532", "port to listen")

var db Database

func main() {
	flag.Parse()
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	r := mux.NewRouter()
	r.HandleFunc(
		"/api/task", submitTaskHandler,
	).Methods("POST")
	r.HandleFunc(
		"/api/task/next", getNextReadyTaskHandler,
	).Methods("GET")
	r.HandleFunc(
		"/api/task/{taskID}", getTaskHandler,
	).Methods("GET")
	r.HandleFunc(
		"/api/task/{taskID}", updateTaskStatusHandler,
	).Methods("PUT")
	r.HandleFunc(
		"/api/task/{taskID}/artifact", postArtifactHandler,
	).Methods("POST")
	r.HandleFunc(
		"/api/task/{taskID}/artifact", getArtifactHandler,
	).Methods("GET")
	http.Handle("/", r)

	initializeDatabase()
	startServer(r)
}

func initializeDatabase() {
	db = &SQLite{}
	err := db.Init()
	if err != nil {
		log.Fatal(err)
	}
}

func startServer(r *mux.Router) {
	server := &http.Server{
		Addr:         getPort(),
		Handler:      r,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Fatal(server.ListenAndServe())
}

func getPort() string {
	return ":" + *portFlag
}

func getTaskHandler(w http.ResponseWriter, r *http.Request) {
	taskID := mux.Vars(r)["taskID"]

	taskIDInt, err := strconv.Atoi(taskID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	task, err := db.GetTask(int64(taskIDInt))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(task)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	return
}

func submitTaskHandler(w http.ResponseWriter, r *http.Request) {
	var task protocol.Task
	err := json.NewDecoder(r.Body).Decode(&task)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	task.CreatedAt = time.Now()
	task.UpdatedAt = time.Now()
	task.Status = protocol.TaskStatusWaitingForUploadingArtifacts

	task.ID, err = db.AddTask(task)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("{\"task_id\": %d}", task.ID)))
}

func updateTaskStatusHandler(w http.ResponseWriter, r *http.Request) {
	taskID := mux.Vars(r)["taskID"]

	taskIDInt, err := strconv.Atoi(taskID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var task protocol.Task
	err = json.NewDecoder(r.Body).Decode(&task)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = updateTaskStatus(int64(taskIDInt), task.Status)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// Receives the artifact from the post request as a zip file and unzip it.
func postArtifactHandler(w http.ResponseWriter, r *http.Request) {
	artifactPath := "./akita/artifacts/"
	taskID := mux.Vars(r)["taskID"]

	taskIDInt, err := strconv.Atoi(taskID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	err = os.MkdirAll(artifactPath+taskID, 0755)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	fmt.Printf("Receiving artifact for task %s\n", taskID)
	err = r.ParseMultipartForm(32 << 20)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	file, _, err := r.FormFile("file")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	defer file.Close()

	f, err := os.OpenFile(artifactPath+taskID+"/artifact.zip", os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	defer f.Close()

	_, err = io.Copy(f, file)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	fmt.Printf("Received artifact for task %s\n", taskID)

	err = updateTaskStatus(int64(taskIDInt), protocol.TaskStatusReady)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
}

func getArtifactHandler(w http.ResponseWriter, r *http.Request) {
	taskID := mux.Vars(r)["taskID"]

	taskIDInt, err := strconv.Atoi(taskID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	_, err = db.GetTask(int64(taskIDInt))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	artifactPath := "./akita/artifacts/" + taskID + "/"
	zipFile, err := os.Open(artifactPath + "artifact.zip")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	defer zipFile.Close()

	w.Header().Set("Content-Type", "application/zip")
	w.Header().Set("Content-Disposition", "attachment; filename=artifact.zip")

	_, err = io.Copy(w, zipFile)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	return
}

func updateTaskStatus(taskID int64, status protocol.TaskStatus) error {
	task := protocol.Task{
		ID:     taskID,
		Status: status,
	}

	return db.UpdateTask(task)
}

func getNextReadyTaskHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("Req: %s\n", r.URL.String())

	task, found, err := db.GetNextTask()
	if err != nil {
		log.Printf("%d: %s\n", http.StatusInternalServerError, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	if !found {
		log.Printf("%d\n", http.StatusNoContent)
		w.WriteHeader(http.StatusNoContent)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	var buffer bytes.Buffer
	err = json.NewEncoder(&buffer).Encode(task)
	if err != nil {
		log.Printf("%d: %s\n", http.StatusInternalServerError, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	err = updateTaskStatus(task.ID, protocol.TaskStatusRunning)
	if err != nil {
		log.Printf("%d: %s\n", http.StatusInternalServerError, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	log.Printf("%d: %s\n", http.StatusOK, buffer.String())
	w.Write(buffer.Bytes())
}
