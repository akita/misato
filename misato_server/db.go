package main

import "gitlab.com/akita/misato/protocol"

// Database provides an abstraction of database operations
type Database interface {
	Init() error
	AddTask(task protocol.Task) (int64, error)
	UpdateTask(task protocol.Task) error
	GetTask(id int64) (protocol.Task, error)
	GetNextTask() (protocol.Task, bool, error)
}
