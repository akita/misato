package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"strings"
	"time"

	"gitlab.com/akita/misato/protocol"
	"gitlab.com/akita/misato/zipping"
)

var taskID int
var artifactPath string
var command string

func init() {
	cacheDir, err := os.UserCacheDir()
	if err != nil {
		fmt.Println("Failed to get user cache dir:", err)
		os.Exit(1)
	}

	artifactPath = cacheDir + "/akita/artifacts/"
}

func main() {
	flag.Parse()

	fmt.Println(flag.NArg(), flag.Args())
	command = strings.Join(flag.Args(), " ")

	err := postTask()
	if err != nil {
		panic(err)
	}

	err = uploadArtifact()
	if err != nil {
		panic(err)
	}

	err = waitForTaskToComplete()
	if err != nil {
		panic(err)
	}

	err = downloadArtifact()
	if err != nil {
		panic(err)
	}

}

func getServerURL() string {
	url := os.Getenv("MISATO_SERVER_URL")

	if url == "" {
		panic("MISATO_SERVER_URL is not set")
	}

	return url
}

func waitForTaskToComplete() error {
	for {
		status := getTaskStatus()
		if status == "completed" {
			break
		}
		time.Sleep(time.Second * 5)
	}

	return nil
}

func getTaskStatus() string {
	rsp, err := http.Get(fmt.Sprintf("%s/api/task/%d",
		getServerURL(), taskID))
	if err != nil {
		panic(err)
	}

	defer rsp.Body.Close()

	content, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(content))

	var rspBody protocol.Task
	err = json.Unmarshal(content, &rspBody)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Task Status: %s\n", rspBody.Status)

	return rspBody.Status.String()
}

type postTaskRspBody struct {
	TaskID int `json:"task_id"`
}

func postTask() error {
	task := protocol.Task{
		Command: command,
	}
	taskData, err := json.Marshal(task)
	if err != nil {
		return err
	}

	rsp, err := http.Post(
		getServerURL()+"/api/task",
		"application/json",
		bytes.NewBuffer(taskData),
	)
	if err != nil {
		return err
	}

	defer rsp.Body.Close()

	content, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return err
	}

	if rsp.StatusCode != http.StatusOK {
		return fmt.Errorf("Failed to post task: %d\n%s",
			rsp.StatusCode, content)
	}

	var rspBody postTaskRspBody
	fmt.Println(string(content))
	err = json.Unmarshal(content, &rspBody)
	if err != nil {
		return err
	}

	taskID = rspBody.TaskID
	fmt.Printf("Task Created, ID: %d\n", taskID)

	return nil
}

func uploadArtifact() error {
	artifactFullPath := fmt.Sprintf("%s/artifact/%d/in.zip",
		artifactPath, taskID)

	err := zipping.Zip(".", artifactFullPath, "")
	if err != nil {
		return err
	}

	file, err := os.Open(artifactFullPath)
	if err != nil {
		return err
	}

	defer file.Close()

	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	fw, err := w.CreateFormFile("file", "in.zip")
	if err != nil {
		return err
	}

	_, err = io.Copy(fw, file)
	if err != nil {
		return err
	}

	err = w.Close()
	if err != nil {
		return err
	}

	contentType := w.FormDataContentType()
	rsp, err := http.Post(
		fmt.Sprintf("%s/api/task/%d/artifact", getServerURL(), taskID),
		contentType,
		&b,
	)
	if err != nil {
		return err
	}

	defer rsp.Body.Close()

	content, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return err
	}

	if rsp.StatusCode != http.StatusOK {
		return fmt.Errorf("Failed to upload artifact: %d\n%s",
			rsp.StatusCode, content)
	}

	return nil
}

func downloadArtifact() error {
	rsp, err := http.Get(fmt.Sprintf("%s/api/task/%d/artifact",
		getServerURL(), taskID))
	if err != nil {
		return err
	}

	defer rsp.Body.Close()

	content, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return err
	}

	if rsp.StatusCode != http.StatusOK {
		return fmt.Errorf("Failed to download artifact: %d\n%s",
			rsp.StatusCode, content)
	}

	outArtifactDir := fmt.Sprintf("%s/%d", artifactPath, taskID)
	err = os.MkdirAll(outArtifactDir, 0755)
	if err != nil {
		return err
	}

	outArtifactFileName := outArtifactDir + "/out.zip"
	err = ioutil.WriteFile(outArtifactFileName, content, 0644)
	if err != nil {
		return err
	}

	err = zipping.Unzip(outArtifactFileName, ".")
	if err != nil {
		return err
	}

	return nil
}
