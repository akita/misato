package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"os/exec"
	"runtime/debug"
	"strings"
	"sync/atomic"
	"time"

	"gitlab.com/akita/misato/protocol"
	"gitlab.com/akita/misato/zipping"
)

var state State
var artifactPath string

func init() {
	cacheDir, err := os.UserCacheDir()
	if err != nil {
		fmt.Println("Failed to get user cache dir:", err)
		os.Exit(1)
	}

	artifactPath = cacheDir + "/akita/artifacts/"
}

type State struct {
	numRunningTask int32
}

func main() {
	flag.Parse()
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	for {
		mainLoop()

		time.Sleep(time.Second)
	}
}

func mainLoop() {
	defer func() {
		if r := recover(); r != nil {
			log.Println(r)
			debug.PrintStack()
		}
	}()

	if canAcceptTask() {
		task, found := fetchTask()
		if !found {
			return
		}

		downloadArtifacts(task)

		atomic.AddInt32(&state.numRunningTask, 1)

		runTask(task)

		uploadArtifacts(task)

		markTaskAsCompleted(task)
	}
}

func canAcceptTask() bool {
	return true
}

func fetchTask() (protocol.Task, bool) {
	url := fmt.Sprintf("%s/api/task/next", getServerURL())
	log.Println("GET: ", url)

	rsp, err := http.Get(url)
	if err != nil {
		panic(err)
	}

	defer rsp.Body.Close()

	log.Printf("%d: ", rsp.StatusCode)

	if rsp.StatusCode == http.StatusNoContent {
		return protocol.Task{}, false
	}

	content, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		panic(err)
	}

	log.Println(string(content))

	var rspBody protocol.Task
	err = json.Unmarshal(content, &rspBody)
	if err != nil {
		panic(err)
	}

	return rspBody, true
}

func downloadArtifacts(task protocol.Task) {
	rsp, err := http.Get(fmt.Sprintf("%s/api/task/%d/artifact",
		getServerURL(), task.ID))
	if err != nil {
		panic(err)
	}

	defer rsp.Body.Close()

	err = os.MkdirAll(fmt.Sprintf("./akita/tasks/%d", task.ID), 0755)
	if err != nil {
		panic(err)
	}

	outZip := fmt.Sprintf("./akita/tasks/%d/artifact.zip", task.ID)
	outFile, err := os.Create(outZip)
	if err != nil {
		panic(err)
	}

	defer outFile.Close()

	_, err = io.Copy(outFile, rsp.Body)
	if err != nil {
		panic(err)
	}

	err = zipping.Unzip(
		outZip, fmt.Sprintf("./akita/tasks/%d/", task.ID))
	if err != nil {
		panic(err)
	}

	err = os.Remove(outZip)
	if err != nil {
		panic(err)
	}
}

func uploadArtifacts(task protocol.Task) {
	taskWorkingPath := fmt.Sprintf("./akita/tasks/%d", task.ID)
	artifactFullPath := fmt.Sprintf("./akita/artifacts/%d/out.zip", task.ID)

	err := zipping.Zip(taskWorkingPath, artifactFullPath,
		fmt.Sprintf("akita/tasks/%d/", task.ID))
	if err != nil {
		panic(err)
	}

	file, err := os.Open(artifactFullPath)
	if err != nil {
		panic(err)
	}

	defer file.Close()

	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	fw, err := w.CreateFormFile("file", "out.zip")
	if err != nil {
		panic(err)
	}

	_, err = io.Copy(fw, file)
	if err != nil {
		panic(err)
	}

	err = w.Close()
	if err != nil {
		panic(err)
	}

	contentType := w.FormDataContentType()
	rsp, err := http.Post(
		fmt.Sprintf("%s/api/task/%d/artifact", getServerURL(), task.ID),
		contentType,
		&b,
	)
	if err != nil {
		panic(err)
	}

	defer rsp.Body.Close()

	content, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		panic(err)
	}

	if rsp.StatusCode != http.StatusOK {
		panic(fmt.Sprintf("Failed to upload artifact: %d\n%s",
			rsp.StatusCode, content))
	}
}

func runTask(task protocol.Task) {
	tokens := strings.Split(task.Command, " ")
	enableExecutionPermission(tokens[0], task)
	executeCommand(task, tokens)
}

func executeCommand(task protocol.Task, tokens []string) {
	stdoutFileName := fmt.Sprintf("./akita/tasks/%d/stdout", task.ID)
	stdoutFile, err := os.Create(stdoutFileName)
	if err != nil {
		panic(err)
	}

	defer stdoutFile.Close()

	stderrFileName := fmt.Sprintf("./akita/tasks/%d/stderr", task.ID)
	stderrFile, err := os.Create(stderrFileName)
	if err != nil {
		panic(err)
	}

	defer stderrFile.Close()

	cmd := &exec.Cmd{
		Path:   tokens[0],
		Args:   tokens,
		Env:    []string{},
		Dir:    fmt.Sprintf("./akita/tasks/%d", task.ID),
		Stdin:  nil,
		Stdout: stdoutFile,
		Stderr: stderrFile,
	}

	err = cmd.Run()
	if err != nil {
		panic(err)
	}

	atomic.AddInt32(&state.numRunningTask, -1)
}

func markTaskAsCompleted(task protocol.Task) {
	finalTask := protocol.Task{
		ID:     task.ID,
		Status: protocol.TaskStatusCompleted,
	}
	finalTaskJson, err := json.Marshal(finalTask)
	if err != nil {
		panic(err)
	}

	req, err := http.NewRequest(
		http.MethodPut,
		fmt.Sprintf("%s/api/task/%d", getServerURL(), task.ID),
		bytes.NewReader(finalTaskJson))
	if err != nil {
		panic(err)
	}

	req.Header.Set("Content-Type", "application/json")

	rsp, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}

	defer rsp.Body.Close()
}

func enableExecutionPermission(executable string, task protocol.Task) {
	cmd1 := exec.Command("chmod", "+x", executable)
	cmd1.Dir = fmt.Sprintf("./akita/tasks/%d", task.ID)
	err := cmd1.Run()
	if err != nil {
		panic(err)
	}
}

func getServerURL() string {
	url := os.Getenv("MISATO_SERVER_URL")

	if url == "" {
		panic("MISATO_SERVER_URL is not set")
	}

	return url
}
