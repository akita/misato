// Package zipping provides functions for zipping and unzipping files.
package zipping

import (
	"archive/zip"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

// Zip compresses a folder into a zip file.
func Zip(src, dst string, srcPrefix string) error {
	log.Printf("Zipping %s to %s", src, dst)

	dstPath := filepath.Dir(dst)

	err := os.MkdirAll(dstPath, 0755)
	if err != nil {
		return err
	}

	zipFile, err := os.Create(dst)
	if err != nil {
		return err
	}

	defer zipFile.Close()

	zipWriter := zip.NewWriter(zipFile)
	err = filepath.Walk(src,
		func(path string, info os.FileInfo, err error) error {
			return addFileToZip(zipWriter, path, srcPrefix, info, err)
		})
	if err != nil {
		return err
	}

	err = zipWriter.Close()
	if err != nil {
		return err
	}

	return nil
}

func addFileToZip(
	zipWriter *zip.Writer,
	path, pathPrefix string,
	info os.FileInfo,
	err error,
) error {
	fmt.Printf("Adding %s to zip\n", path)

	if err != nil {
		return err
	}

	if info.IsDir() {
		return nil
	}

	file, err := os.Open(path)
	if err != nil {
		return err
	}

	defer file.Close()

	f, err := zipWriter.Create(strings.TrimPrefix(path, pathPrefix))
	if err != nil {
		return err
	}

	_, err = io.Copy(f, file)
	if err != nil {
		return err
	}

	return nil
}

// Unzip decompresses a zip file into a folder.
func Unzip(src, dst string) error {
	reader, err := zip.OpenReader(src)
	if err != nil {
		return err
	}

	dst, err = filepath.Abs(dst)
	if err != nil {
		return err
	}

	err = os.MkdirAll(dst, 0755)
	if err != nil {
		return err
	}

	for _, file := range reader.File {
		err = extractFile(file, dst)
		if err != nil {
			fmt.Println(err)
		}
	}

	return nil
}

func extractFile(file *zip.File, destination string) error {
	path := filepath.Join(destination, file.Name)

	if file.FileInfo().IsDir() {
		err := os.MkdirAll(path, file.Mode())
		if err != nil {
			return err
		}

		return nil
	}

	err := os.MkdirAll(filepath.Dir(path), 0755)
	if err != nil {
		return err
	}

	fileReader, err := file.Open()
	if err != nil {
		return err
	}

	defer fileReader.Close()

	fileWriter, err := os.OpenFile(path,
		os.O_WRONLY|os.O_CREATE|os.O_TRUNC, file.Mode())
	if err != nil {
		return err
	}

	defer fileWriter.Close()

	_, err = io.Copy(fileWriter, fileReader)
	if err != nil {
		return err
	}

	return nil
}
